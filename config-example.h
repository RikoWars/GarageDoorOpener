// WiFi settings
const char* ssid = "SSID";
const char* password = "PSK";
char* espHostname = "Hostname";

// MQTT server
const char* mqtt_server = "MQTT broker hostname, FQDN or IP address";
int mqtt_server_port = 1883;

// MQTT subscribe
const char* mainTopic = "Topic/#";

// MQTT last will and testament
byte willQoS = 0;
const char* willTopic = "lwt/esp-topic";
const char* willOnMessage = "online";
const char* willOffMessage = "offline";
const char* willClientID = "esp-ClientID";
boolean willRetain = true;

//Relay
const int relay = D5;
